# How to run this project



- Install all the dependencies listed in "./requirements.txt"

        pip install -r requirements.txt
    
- Run the django project from "./nar-db/ folder". The port can be specified within
  the running command or changing "./nar-db/manage.py" file at line 19.
  
        python3 manage.py runserver 127.0.0.1:8000
   
   
    
    
