#!/usr/bin/env python3

import pandas as pd
import json
import requests
with open('mutations.json', 'r') as f:
    data = json.load(f)
url = "https://hpmp.esat.kuleuven.be/api/"

chunks = [data[100*i:100*(i+1)] for i in range(len(data)//100 + 1)]
for i, entry in enumerate(chunks):
    print(str(i / float(len(chunks))))
    r = requests.post(url, json=entry, auth=('admin', 'GattoRatto1917'))
    print(r)
