#!/usr/bin/env python3
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

titles = ['Interaction', 'Binding',
       'Function', 'Phosphorylation', 'Ubiquination', 'Glycosylation',
       'Sumoylation', 'Acetylation', 'Other PTM', 'Methylation',
       'Ribosylation', 'Subcell Localization', 'Mimetic PTM', 'Folding',
       'Expression']
# Plots for website
# sns.set_theme(style="darkgrid")
df = pd.read_csv("./mutations4.tsv", sep='\t')
print(df.head())
print(df.keys())
df_annot = df[df.keys().tolist()[7:]]
# print(df_annot)
df_annot_hist = df_annot.count(axis=0)
print(df_annot_hist)
df_annot_hist.plot(kind="bar", ylabel="Number of Occurrences")
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.grid(linestyle="--")
plt.savefig("annot_num_distribution.png", dpi=300)
print(df_annot_hist[0])

axes = df_annot.hist(bins=[-1.1, -0.9, -0.1, 0.1, 0.9, 1.1], layout=(3,5))
print(axes)
for i, sub in enumerate(axes):
    for j, ax in enumerate(sub):
        ax.set_title(titles[5 * i + j])
        if i == 2:
            ax.set_xlabel("Labels")
        if j == 0:
            ax.set_ylabel("Number Occurrences")
        ax.grid(linestyle='--')
figure = plt.gcf()
figure.set_size_inches(13, 9);
plt.savefig("multiplot_annot_distribution.png", dpi=300)
