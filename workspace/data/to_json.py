#!/usr/bin/env python3

import pandas as pd

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except:
        return False

df = pd.read_csv("./mutations4.tsv", sep='\t')
print(df.head())
#df = df.loc[df.function.str.isnumeric()]
df.to_json(path_or_buf="./mutations.json", orient="records")


