var res;
var db_data;
var table;
var ptm_filter;
var forced_switch = false;
var res_abs_zero;
var res_abs_null;
var res_zero;
var field_dict = {
    0: "uniprot_id",
    1: "wildtype",
    2: "position",
    3: "mutation",
    4: "pdb_id",
    5: "pdb_position",
    6: "label",
    7: "interaction",
    8: "binding",
    9: "function",
    10: "any_ptm",
    11: "phosphorylation",
    12: "ubiquination",
    13: "glycosylation",
    14: "sumoylation",
    15: "glcnac",
    16: "acetilation",
    17: "other_ptm",
    18: "methilation",
    19: "ribosylation",
    20: "subcell_localization_change",
    21: "allosteric_change",
    22: "folding",
    23: "transcription"
}

$.get(
    "https://hpmp.esat.kuleuven.be/api/",
    function(data) {

        db_data = data;
        data.forEach(function(part, index) {
            delete this[index]["id"];
        }, data);
        console.log(data)
        res = data.map(Object.values);
        res.forEach(function(el){
            el.splice(10, 0 , Math.min(1, el.slice(10,18).map(Math.abs).reduce((s, v) => s + (v || 0), 0)));
        });
        $('#db').show()
        table = $('#db').DataTable( {
            data: res,
        } );
        var col_ptm = table.column(10);
        col_ptm.visible(false);

        $("#collapseSwitch").switchButton({
  checked: false
        });

    buildAlternativeDatas(res);
    }
);

$(document).ready(function() {
    $("#query-db").trigger("reset");
    $(".form-check").click(function(){
        var col = table.column(0);
        // col.visible(!col.visible());
        $('#checkboxes input[type=checkbox]').each(function (i) {
                    console.log(i);
            if($("#collapseSwitch:checkbox:checked").length > 0){
                if(this.checked && i >= 11 && i < 18){
                    console.log("PORCODIO");
                    console.log(i);
                    console.log(this.checked);
                    forced_switch = true;
                    $("#collapseSwitch").switchButton({checked:false});
                }
            }
            col = table.column(i);
            console.log(this.checked);
            if(this.checked && !(col.visible())){
                col.visible(true);
            }
            else if(!(this.checked) && col.visible()){
                col.visible(false);
            }

        });
    });
});

function shortenedCol(arrayofarray, indexlist) {
    return arrayofarray.map(function (array) {
        return indexlist.map(function (idx) {
            return array[idx];
        });
    });
}

function absValue(){
    var to_zero = $("#toggle-non-zero").hasClass("btn-danger")
    var abs = $("#toggle-abs").hasClass("btn-primary")
    if (abs){
        $("#toggle-abs").removeClass("btn-primary").addClass("btn-danger");
        $("#toggle-abs").html("Deactivate Abs Values");
        if(to_zero){
            console.log("abs , yes zero");
            table.clear();
            table.rows.add(res_abs_zero);
            table.draw();
        }
        else{
            console.log("yes abs , no zero");
            table.clear();
            table.rows.add(res_abs_null);
            table.draw();
        }
    }
    else{
        $("#toggle-abs").removeClass("btn-danger").addClass("btn-primary");
        $("#toggle-abs").html("Activate Abs Values");
        if(to_zero){
            console.log("no abs , yes zero");
            table.clear();
            table.rows.add(res_zero);
            table.draw();
        }
        else{
            table.clear();
            table.rows.add(res);
            table.draw();
        }

    }
}
function nullToZero(){
    var to_zero = $("#toggle-non-zero").hasClass("btn-primary")
    var abs = $("#toggle-abs").hasClass("btn-danger")
    if (to_zero){
        $("#toggle-non-zero").removeClass("btn-primary").addClass("btn-danger");
        $("#toggle-non-zero").html("Deactivate Null to Zero");
        if(abs){
            console.log("abs , yes zero");
            table.clear();
            table.rows.add(res_abs_zero);
            table.draw();
        }
        else{
            console.log("No abs , yes zero");
            table.clear();
            table.rows.add(res_zero);
            table.draw();
        }
    }
    else{
        $("#toggle-non-zero").removeClass("btn-danger").addClass("btn-primary");
        $("#toggle-non-zero").html("Activate Null to Zero");
        if(abs){
            console.log("abs , no zero");
            table.clear();
            table.rows.add(res_abs_null);
            table.draw();
        }
        else{
            table.clear();
            table.rows.add(res);
            table.draw();
        }

    }
}

function buildAlternativeDatas(res){
    res_zero = res.slice();
    res_zero.forEach(function(e,i){
        res_zero[i] = res_zero[i].map(
            function(val, k) {
                return val == null ? 0 : val;
            })
    });

    res_abs_zero = res_zero.slice();
    res_abs_zero.forEach(function(e,i){
        res_abs_zero[i] = res_abs_zero[i].map(
            function(val, k) {
                if(k >= 7)
                    return Math.abs(val);
                else
                    return val;
            })
    });

    res_abs_null = res.slice();
    res_abs_null.forEach(function(e,i){
        res_abs_null[i] = res_abs_null[i].map(
            function(val, k) {
                if(k >= 7 && val != null)
                    return Math.abs(val);
                else
                    return val;
            })
    });


}

function filterObject(){
    var to_keep = [];
    var keys = [];
    $('#checkboxes input[type=checkbox]').each(function (i) {
        var col = table.column(i);
        if (col.visible()){
            to_keep.push(i);
            keys.push(field_dict[i]);
        }
    });
    var data_table = table.rows( { search: 'applied' } ).data();
    var data_array = [];
    data_table.each(function(e){data_array.push(e)});
    var data_table_filtered = shortenedCol(data_array, to_keep);
    var zipped_data = data_table_filtered.map(a => a.map(function(e, i) {
        return [keys[i], e];
    }));
    tmp = zipped_data;
    var final_data = zipped_data.map(a => Object.fromEntries(a));
    return [final_data, keys];
}


function downloadTSV(){
    let filter = filterObject();
    let keys = filter[1];
    let ress = filter[0].map(Object.values);
    console.log(ress);
    ress.unshift(keys);
    let text = ress.map(e => e.join("\t")).join("\n");
    download("database.tsv", text);
}

function downloadJSON(){
    let filter = filterObject();
    let keys = filter[1];
    let ress = filter[0];
    let text = JSON.stringify(ress, null, 2);
    download("database.json", text)
}


function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

$(document).ready(function() {
$('#uid').on( 'keyup change', function () {
    if ( table.column(0).search() !== this.value ) {
                table
                    .column(0)
                    .search( this.value )
                    .draw();
            }
        } );

$('#wildtype').on( 'keyup change', function () {
    if ( table.column(1).search() !== this.value ) {
                table
                    .column(1)
                    .search( this.value )
                    .draw();
            }
        } );
$('#position').on( 'keyup change', function () {
    if ( table.column(2).search() !== this.value ) {
                table
                    .column(2)
                    .search( this.value )
                    .draw();
            }
        } );
$('#mutation').on('input', function () {
    if ( table.column(3).search() !== this.value ) {
                table
                    .column(3)
                    .search( this.value )
                    .draw();
            }
});
$('#pdb').on( 'keyup change', function () {
    if ( table.column(4).search() !== this.value ) {
                table
                    .column(4)
                    .search( this.value )
                    .draw();
            }
        } );

$('#pdb_position').on( 'keyup change', function () {
    if ( table.column(5).search() !== this.value ) {
                table
                    .column(5)
                    .search( this.value )
                    .draw();
            }
        } );

$('#label').on( 'keyup change', function () {
    if ( table.column(6).search() !== this.value ) {
                table
                    .column(6)
                    .search( this.value )
                    .draw();
            }
        } );
});

function checkAll(){

    $("#collapseSwitch").switchButton({
            checked: false
    });
  $('#checkboxes input[type=checkbox]').each(function (i) {
     if(i==10){
        this.checked=false;
     }
      else{
      this.checked = true;
      col = table.column(i);
      if(!(col.visible()))
          col.visible(true);
      }
  });
}

function collapseColumns(){
    console.log("Collapsing columns");
    var checked = $("#collapseSwitch:checkbox:checked").length;
    console.log(checked);
    if(forced_switch){
        forced_switch = false;
        return;
    }
    if (checked != 0){
        $('#checkboxes input[type=checkbox]').each(function (i) {
            col = table.column(i);
            if(i==10){
                col.visible(true);
                this.checked = true;
            }
            else if ([11,12,13,14,15,16,17].includes(i)){
                col.visible(false);
                this.checked = false;
            }

        });
    }
    else{
        $('#checkboxes input[type=checkbox]').each(function (i) {
            col = table.column(i);
            if(i==10){
                col.visible(false);
                this.checked = false;
            }
            else if ([11,12,13,14,15,16,17].includes(i)){
                col.visible(true);
                this.checked = true;
            }

        });
    }
}
