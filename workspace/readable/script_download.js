
$.get(
    "https://hpmp.esat.kuleuven.be/api/",
    function(data) {

        data.forEach(function(part, index) {
            delete this[index]["id"];
        }, data);
        db_data = data;
        console.log(data)
        res = data.map(Object.values);
        $('#db').show()
        table = $('#db').DataTable( {
            data: res,
        } );

    }
);



function downloadTSV(){
    let keys = Object.keys(db_data[0]);
    let ress = res;
    ress.unshift(keys);
    let text = ress.map(e => e.join("\t")).join("\n");
    download("database.tsv", text);
}

function downloadJSON(){
    let text = JSON.stringify(db_data, null, 2);
    download("database.json", text)
}


function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
