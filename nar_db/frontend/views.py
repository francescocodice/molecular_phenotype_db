from django.shortcuts import render
import json
from db_api.serializers import protein_mutation_serializer
from db_api.models import protein_mutation
from django.core import serializers
from rest_framework.renderers import JSONRenderer
db_data = None

def index(request):
    return render(request, 'frontend/home.html', {})

def contact_us(request):
    return render(request, 'frontend/contact_us.html', {})

def api_doc(request):
    return render(request, 'frontend/api_doc.html', {})

def download(request):
    mutations = protein_mutation.objects.all()
    db_data = protein_mutation_serializer(mutations, many=True)
    js = json.dumps(db_data.data)
    return render(request, 'frontend/download.html', {'db_data': js})

def browse(request):
    mutations = protein_mutation.objects.all()
    db_data = protein_mutation_serializer(mutations, many=True)
    js = json.dumps(db_data.data)
    return render(request, 'frontend/browse.html', {'db_data': js})
