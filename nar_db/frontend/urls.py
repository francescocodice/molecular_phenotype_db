#!/usr/bin/env python3

from django.conf.urls import include, url
from . import views

app_name = 'frontend'


urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^contact-us/$', views.contact_us, name='contact-us'),
	url(r'^browse/$', views.browse, name='browse'),
	url(r'^download/$', views.download, name='download'),
	url(r'^api-doc/$', views.api_doc, name='api-doc'),

]
