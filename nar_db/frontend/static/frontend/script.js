var res;
var table;
var ptm_filter;
var forced_switch = false;
var res_abs_zero;
var res_abs_null;
var res_zero;
var field_dict = {
    0: "uniprot_id",
    1: "wildtype",
    2: "position",
    3: "mutation",
    4: "pdb_id",
    5: "pdb_position",
    6: "phenotypic_annotation",
    7: "interaction",
    8: "binding",
    9: "function",
    10: "any_ptm",
    11: "phosphorylation",
    12: "ubiquination",
    13: "glycosylation",
    14: "sumoylation",
    15: "acetylation",
    16: "other_ptm",
    17: "methylation",
    18: "ribosylation",
    19: "subcell_localization_change",
    20: "mimetic_ptm",
    21: "folding",
    22: "transcription"
}

$.get(
    "https://hpmp.esat.kuleuven.be/api/",
    function(data) {
    }
);





$(document).ready(function() {

    console.log(db_data);
    $("#query-db").trigger("reset");
    $(".form-check").click(function(){
        var col = table.column(0);
        // col.visible(!col.visible());
        $('#checkboxes input[type=checkbox]').each(function (i) {
            console.log(i);
            if($("#collapseSwitch").hasClass("btn-success")){
                if(i >= 11 && i < 19 && this.checked){
                    $('#collapseSwitch').removeClass("btn-success").addClass("btn-danger");
                    $('#collapseSwitch').html("Activate Collapse");
                }
            }
            col = table.column(i);
            console.log(this.checked);
            if(this.checked && !(col.visible())){
                col.visible(true);
            }
            else if(!(this.checked) && col.visible()){
                col.visible(false);
            }

        });
    });


    db_data.forEach(function(part, index) {
        delete this[index]["id"];
    }, db_data);
    res = db_data.map(Object.values);
    res.forEach(function(el){
        if(el.slice(10,19).some(function(i) { return i !== null; })){
            el.splice(10, 0 , Math.min(1, el.slice(10,19).map(Math.abs).reduce((s, v) => s + (v || 0), 0)));
        }
        else{
            el.splice(10, 0, null);
        }
    });
    $('#db').show()
    table = $('#db').DataTable( {
        data: res,
        lengthmenu: [20, 50, 100, 200, 500],
        "columnDefs": [ {
            "targets": 0,
            "data": null,
            "render": function ( data, type, row, meta ) {
                return '<a target="_blank" rel="noopener noreferrer" href="https://www.uniprot.org/uniprot/' + data[0] + '">' + data[0] + '</a>';
            }
        },
        {
            "targets": 4,
            "data": null,
            "render": function ( data, type, row, meta ) {
                pdb_id = data[4].split(":")[0]
                return '<a target="_blank" rel="noopener noreferrer" href="https://www.rcsb.org/structure/' + pdb_id + '">' + data[4] + '</a>';
            }
        },
                      ]
    } );
    var col_ptm = table.column(10);
    col_ptm.visible(false);

    $('#collapseSwitch').removeClass("btn-success").addClass("btn-danger");
    $('#collapseSwitch').html("Activate Collapse");

    $("#table-loader").remove();;
    buildAlternativeDatas(res);
    checkAll();
});

function shortenedCol(arrayofarray, indexlist) {
    return arrayofarray.map(function (array) {
        return indexlist.map(function (idx) {
            return array[idx];
        });
    });
}

function absValue(){
    var to_zero = $("#toggle-non-zero").hasClass("btn-success")
    var abs = $("#toggle-abs").hasClass("btn-danger")
    if (abs){
        $("#toggle-abs").removeClass("btn-danger").addClass("btn-success");
        $("#toggle-abs").html("Active - Abs Values");
        if(to_zero){
            console.log("abs , yes zero");
            table.clear();
            table.rows.add(res_abs_zero);
            table.draw();
        }
        else{
            console.log("yes abs , no zero");
            table.clear();
            table.rows.add(res_abs_null);
            table.draw();
        }
    }
    else{
        $("#toggle-abs").removeClass("btn-success").addClass("btn-danger");
        $("#toggle-abs").html("Inactive Abs Values");
        if(to_zero){
            console.log("no abs , yes zero");
            table.clear();
            table.rows.add(res_zero);
            table.draw();
        }
        else{
            table.clear();
            table.rows.add(res);
            table.draw();
        }

    }
}
function nullToZero(){
    var to_zero = $("#toggle-non-zero").hasClass("btn-danger")
    var abs = $("#toggle-abs").hasClass("btn-success")
    if (to_zero){
        $("#toggle-non-zero").removeClass("btn-danger").addClass("btn-success");
        $("#toggle-non-zero").html("Active - Null to Zero");
        if(abs){
            console.log("abs , yes zero");
            table.clear();
            table.rows.add(res_abs_zero);
            table.draw();
        }
        else{
            console.log("No abs , yes zero");
            table.clear();
            table.rows.add(res_zero);
            table.draw();
        }
    }
    else{
        $("#toggle-non-zero").removeClass("btn-success").addClass("btn-danger");
        $("#toggle-non-zero").html("Inactive Null to Zero");
        if(abs){
            console.log("abs , no zero");
            table.clear();
            table.rows.add(res_abs_null);
            table.draw();
        }
        else{
            table.clear();
            table.rows.add(res);
            table.draw();
        }

    }
}

function buildAlternativeDatas(res){
    res_zero = res.slice();
    res_zero.forEach(function(e,i){
        res_zero[i] = res_zero[i].map(
            function(val, k) {
                return val == null ? 0 : val;
            })
    });

    res_abs_zero = res_zero.slice();
    res_abs_zero.forEach(function(e,i){
        res_abs_zero[i] = res_abs_zero[i].map(
            function(val, k) {
                if(k >= 7)
                    return Math.abs(val);
                else
                    return val;
            })
    });

    res_abs_null = res.slice();
    res_abs_null.forEach(function(e,i){
        res_abs_null[i] = res_abs_null[i].map(
            function(val, k) {
                if(k >= 7 && val != null)
                    return Math.abs(val);
                else
                    return val;
            })
    });


}

function filterObject(){
    var to_keep = [];
    var keys = [];
    $('#checkboxes input[type=checkbox]').each(function (i) {
        var col = table.column(i);
        if (col.visible()){
            to_keep.push(i);
            keys.push(field_dict[i]);
        }
    });
    var data_table = table.rows( { search: 'applied' } ).data();
    var data_array = [];
    data_table.each(function(e){data_array.push(e)});
    var data_table_filtered = shortenedCol(data_array, to_keep);
    var zipped_data = data_table_filtered.map(a => a.map(function(e, i) {
        return [keys[i], e];
    }));
    tmp = zipped_data;
    var final_data = zipped_data.map(a => Object.fromEntries(a));
    return [final_data, keys];
}


function downloadTSV(){
    let filter = filterObject();
    let keys = filter[1];
    let ress = filter[0].map(Object.values);
    console.log(ress);
    ress.unshift(keys);
    let text = ress.map(e => e.join("\t")).join("\n");
    download("database.tsv", text);
}

function downloadJSON(){
    let filter = filterObject();
    let keys = filter[1];
    let ress = filter[0];
    let text = JSON.stringify(ress, null, 2);
    download("database.json", text)
}


function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

$(document).ready(function() {
    $('#uid').on( 'keyup change', function () {
        if ( table.column(0).search() !== this.value ) {
            table
                .column(0)
                .search( this.value )
                .draw();
        }
    } );

    $('#wildtype').on( 'keyup change', function () {
        if ( table.column(1).search() !== this.value ) {
            table
                .column(1)
                .search( this.value )
                .draw();
        }
    } );
    $('#position').on( 'keyup change', function () {
        if ( table.column(2).search() !== this.value ) {
            table
                .column(2)
                .search( this.value )
                .draw();
        }
    } );
    $('#mutation').on('input', function () {
        if ( table.column(3).search() !== this.value ) {
            table
                .column(3)
                .search( this.value )
                .draw();
        }
    });
    $('#pdb').on( 'keyup change', function () {
        if ( table.column(4).search() !== this.value ) {
            table
                .column(4)
                .search( this.value )
                .draw();
        }
    } );

    $('#pdb_position').on( 'keyup change', function () {
        if ( table.column(5).search() !== this.value ) {
            table
                .column(5)
                .search( this.value )
                .draw();
        }
    } );

    $('#label').on( 'keyup change', function () {
        if ( table.column(6).search() !== this.value ) {
            table
                .column(6)
                .search( this.value )
                .draw();
        }
    } );


    $('#fuzzy-search').on( 'keyup change', function () {
        if ( table.search() !== this.value ) {
            table
                .search( this.value )
                .draw();
        }
    } );

});

function checkAll(){

    $('#collapseSwitch').removeClass("btn-success").addClass("btn-danger");
    $('#collapseSwitch').html("Activate Collapse");
    $('#checkboxes input[type=checkbox]').each(function (i) {
        if(i==10){
            this.checked=false;
        }
        else{
            this.checked = true;
            col = table.column(i);
            if(!(col.visible()))
                col.visible(true);
        }
    });
}

function collapseColumns(){
    console.log("Collapsing columns");
    var checked = $("#collapseSwitch").hasClass("btn-danger");
    console.log("Checked" , checked);
    if(forced_switch){
        forced_switch = false;
        return;
    }
    if (checked){
        console.log("Checked True -> updating buttons and fileds");
        $('#collapseSwitch').removeClass("btn-danger");
        $('#collapseSwitch').addClass("btn-success");
        $('#collapseSwitch').html("Deactivate Collapse");
        $('#checkboxes input[type=checkbox]').each(function (i) {
            col = table.column(i);
            if(i==10){
                col.visible(true);
                this.checked = true;
            }
            else if ([11,12,13,14,15,16,17,18].includes(i)){
                col.visible(false);
                this.checked = false;
            }

        });
    }
    else{
        console.log("Checked false");
        $('#collapseSwitch').removeClass("btn-success").addClass("btn-danger");
        $('#collapseSwitch').html("Activate Collapse");
        $('#checkboxes input[type=checkbox]').each(function (i) {
            col = table.column(i);
            if(i==10){
                col.visible(false);
                this.checked = false;
            }
            else if ([11,12,13,14,15,16,17,18].includes(i)){
                col.visible(true);
                this.checked = true;
            }

        });
    }
}


function resetForm(){
    $('#query-db').trigger('reset');
    $("#uid").trigger("change");
    $("#label").trigger("change");
    $("#pdb").trigger("change");
    $("#pdb_position").trigger("change");
    $("#mutation").trigger("change");
    $("#position").trigger("change");
    $("#wildtype").trigger("change");
}
