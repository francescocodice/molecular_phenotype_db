from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from db_api.serializers import protein_mutation_serializer
from db_api.models import protein_mutation
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import django_filters.rest_framework
from rest_framework.decorators import action

class protein_mutation_view(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = protein_mutation.objects.all()
    serializer_class = protein_mutation_serializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ["id","uniprot_id", "wildtype", "position", "mutation",
                        "pdb_id", "pdb_position", "phenotypic_annotation",
                  "interaction", "binding", "function", "phosphorylation",
                  "ubiquination", "glycosylation", "sumoylation",
                  "acetylation", "other_ptm", "methylation", "ribosylation",
                  "subcell_localization_change", "mimetic_ptm", "folding",
                  "expression"]
    def create(self, request, *args, **kwargs):
        """
        #checks if post request data is an array initializes serializer with many=True
        else executes default CreateModelMixin.create function
        """
        is_many = isinstance(request.data, list)
        if not is_many:
            return super(protein_mutation_view, self).create(request, *args, **kwargs)
        else:
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_permissions(self):
        if self.action == 'list':
            return [permissions.AllowAny()]
        else :
            return [permissions.IsAdminUser()]

    @action(detail=False, methods=['post'])
    def delete_all(self, request):
        protein_mutation.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
