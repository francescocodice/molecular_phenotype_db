from django.db import models

class protein_mutation(models.Model):
    uniprot_id = models.CharField(max_length=6)
    wildtype = models.CharField(max_length=1)
    position = models.IntegerField()
    mutation = models.CharField(max_length=20)
    pdb_id = models.CharField(max_length=8)
    pdb_position = models.IntegerField()
    phenotypic_annotation = models.CharField(max_length=2000)

    # Manual Annotations
    interaction = models.IntegerField(blank=True, null=True)
    binding = models.IntegerField(blank=True, null=True)
    function = models.IntegerField(blank=True, null=True)
    phosphorylation = models.IntegerField(blank=True, null=True)
    ubiquination = models.IntegerField(blank=True, null=True)
    glycosylation = models.IntegerField(blank=True, null=True)
    sumoylation = models.IntegerField(blank=True, null=True)
    acetylation = models.IntegerField(blank=True, null=True)
    other_ptm = models.IntegerField(blank=True, null=True)
    methylation = models.IntegerField(blank=True, null=True)
    ribosylation = models.IntegerField(blank=True, null=True)
    subcell_localization_change = models.IntegerField(blank=True, null=True)
    mimetic_ptm = models.IntegerField(blank=True, null=True)
    folding = models.IntegerField(blank=True, null=True)
    expression = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.UID
