#!/usr/bin/env python3

from django.contrib.auth.models import User, Group
from rest_framework import serializers
from db_api.models import protein_mutation


class protein_mutation_serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = protein_mutation
        fields = ["id", "uniprot_id", "wildtype", "position", "mutation", "pdb_id",
                  "pdb_position", "phenotypic_annotation", "interaction", "binding", "function",
                  "phosphorylation",
                  "ubiquination", "glycosylation", "sumoylation",
                  "acetylation", "other_ptm", "methylation", "ribosylation",
                  "subcell_localization_change", "mimetic_ptm", "folding",
                  "expression"]
        extra_kwargs = {'interaction': {'allow_null': True},
                        "binding": {'allow_null': True},
                        "function": {'allow_null': True},
                        "phosphorylation": {'allow_null': True},
                        "ubiquination": {'allow_null': True},
                        "glycosylation": {'allow_null': True},
                        "sumoylation": {'allow_null': True},
                        "acetylation": {'allow_null': True},
                        "other_ptm": {'allow_null': True},
                        "methylation": {'allow_null': True},
                        "ribosylation": {'allow_null': True},
                        "subcell_localization_change": {'allow_null': True},
                        "mimetic_ptm": {'allow_null': True},
                        "folding": {'allow_null': True},
                        "expression": {'allow_null': True}}
